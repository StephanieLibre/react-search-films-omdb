import React, {useState} from 'react';
import logo from './logo.svg';
import './App.css';
import Axios from "axios";
// import films from "./films.json"
function App() {
  const [error, setError] = useState("")
  const [films, setFilms] = useState([])
  function displayFilms() {
      if(error) {
          return <div>{error}</div>
      }
      return films.map((film, i) => <li className="filmItem" key={i}><img src={film.Poster} alt=""/> <p>{film.Title} ({film.Year})</p></li>)
  }
  function changeSearch(event) {
      Axios.get("http://www.omdbapi.com", {
          params: {
              apikey: "756998dc",
              s: event.target.value
          }
      })
          .then(response => {
                  if (response.data.Search) {
                      setFilms(response.data.Search)
                      setError("")
                  } else {
                      setError("Aucun résultat")
                  }
              }
          )
  }
  return (
    <div className="App">
      <input type="text" onChange={changeSearch} />
     <ul>{displayFilms()}</ul>
    </div>
  );
}

export default App;
